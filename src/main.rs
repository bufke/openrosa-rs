use axum::{
    extract,
    routing::get,
    Router,
    http::StatusCode,
    response::{IntoResponse},
};

use serde::{Deserialize, Serialize};
use quick_xml::se::to_string;


#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct xform {
    formID: String,
    name: String,
    majorMinorVersion: String,
    version: String,
    hash: String,
    descriptionText: String,
    downloadUrl: String,
    manifestUrl: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct xforms {
    xform: Vec<xform>,
}


#[tokio::main]
async fn main() {
    // build our application with a single route
    let app = Router::new()
      .route("/", get(|| async { "Hello, World!" }))
      .route("/aa/formList", get(form_list).head(send_nothing))
      .route("/aa/xformsManifest/1", get(xform_manifest))
      .route("/aa/submission", get(send_nothing).post(submission).head(send_nothing))
      .route("/aa/forms/1/form.xml", get(form_detail))
      ;

    let app = app.fallback(handler_404);

    // run it with hyper on localhost:8001
    axum::Server::bind(&"0.0.0.0:8001".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn handler_404() -> impl IntoResponse {
    println!("404 not found!");
    (StatusCode::NOT_FOUND, "nothing to see here")
}

async fn form_list() -> String {
    println!("form list");
    let xform = xform {
        formID: String::from("aMaQxPquh5nozNwVYydEWa"),
        name: String::from("eh"),
        majorMinorVersion: String::from("1"),
        version: String::from("1"),
        hash: String::from("md5:cc368602e8c26cfde04b39d270fd7d5d"),
        descriptionText: String::from("eh"),
        downloadUrl: String::from("http://kc.kobo.local:8001/aa/forms/1/form.xml"),
        manifestUrl: String::from("http://kc.kobo.local:8001/aa/xformsManifest/1")
    };

    let xforms = xforms {
        xform: vec![xform]
    };

    let mut response_xml: String = "<?xml version= \"1.0\" encoding= \"utf-8\"?>".to_owned();
    let xml = to_string(&xforms).unwrap();
    // xml
    response_xml.push_str(&xml);
    // response_xml

    // Json(xform)
    "<?xml version=\"1.0\" encoding=\"utf-8\"?><xforms xmlns=\"http://openrosa.org/xforms/xformsList\"><xform><formID>aMaQxPquh5nozNwVYydEWa</formID><name>eh</name><majorMinorVersion>1 (2023-05-19 18:18:43)</majorMinorVersion><version>1 (2023-05-19 18:18:43)</version><hash>md5:cc368602e8c26cfde04b39d270fd7d5d</hash><descriptionText>eh</descriptionText><downloadUrl>http://kc.kobo.local:8001/aa/forms/1/form.xml</downloadUrl><manifestUrl>http://kc.kobo.local:8001/aa/xformsManifest/1</manifestUrl></xform></xforms>".to_string()

}

async fn send_nothing() -> &'static str {
    println!("send nothing!");
    ""
}

async fn xform_manifest() -> &'static str {
    println!("xform manifest");
    "<?xml version=\"1.0\" encoding=\"utf-8\"?><manifest xmlns=\"http://openrosa.org/xforms/xformsManifest\"></manifest>"
}

async fn submission(body: String) -> StatusCode {
  println!("{:?}", body);
  StatusCode::CREATED
}

async fn form_detail() -> &'static str {
    println!("form detail");
    r###"<?xml version="1.0" encoding="utf-8"?>
<h:html xmlns="http://www.w3.org/2002/xforms" xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:h="http://www.w3.org/1999/xhtml" xmlns:jr="http://openrosa.org/javarosa" xmlns:odk="http://www.opendatakit.org/xforms" xmlns:orx="http://openrosa.org/xforms" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <h:head>
    <h:title>eh</h:title>
    <model odk:xforms-version="1.0.0">
      <instance>
        <aMaQxPquh5nozNwVYydEWa id="aMaQxPquh5nozNwVYydEWa" version="1 (2023-05-19 18:18:43)">
          <formhub>
            <uuid/>
          </formhub>
          <start/>
          <end/>
          <fsd/>
          <__version__/>
          <meta>
            <instanceID/>
          </meta>
        </aMaQxPquh5nozNwVYydEWa>
      </instance>
      <bind jr:preload="timestamp" jr:preloadParams="start" nodeset="/aMaQxPquh5nozNwVYydEWa/start" type="dateTime"/>
      <bind jr:preload="timestamp" jr:preloadParams="end" nodeset="/aMaQxPquh5nozNwVYydEWa/end" type="dateTime"/>
      <bind nodeset="/aMaQxPquh5nozNwVYydEWa/fsd" required="false()" type="string"/>
      <bind calculate="'vHH6pSi5RwNTsvkQYsj5Df'" nodeset="/aMaQxPquh5nozNwVYydEWa/__version__" type="string"/>
      <bind jr:preload="uid" nodeset="/aMaQxPquh5nozNwVYydEWa/meta/instanceID" readonly="true()" type="string"/>
      <bind nodeset="/aMaQxPquh5nozNwVYydEWa/formhub/uuid" type="string" calculate="'9ec48fa015e2490389ef006b78d36738'"/>
    </model>
  </h:head>
  <h:body>
    <select1 ref="/aMaQxPquh5nozNwVYydEWa/fsd">
      <label>fsd</label>
      <item>
        <label>Option 1</label>
        <value>option_1</value>
      </item>
      <item>
        <label>Option 2</label>
        <value>option_2</value>
      </item>
    </select1>
  </h:body>
</h:html>"###
}